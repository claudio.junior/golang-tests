package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// @BasePath /api/v1
// @Summary Creating oppening
// @Description Create a new job opening
// @Tags Openings
// @Accept json
// @Produce json
// @Success 200
// @Router /opening [post]
func CreateOpeningHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"msg": "created",
	})
}
