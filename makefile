.PHONY: default run build docs clean



#tasks
default: run

run:
	@go run main.go
run-with-docs:
	@swag init
	@go run main.go
docs:
	@swag init